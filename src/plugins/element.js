import Vue from 'vue'
import {
    Button,
    Card,
    Checkbox,
    CheckboxButton,
    CheckboxGroup,
    Collapse,
    CollapseItem,
    Row,
    TabPane,
    Tabs
} from 'element-ui'
import lang from 'element-ui/lib/locale/lang/fr'
import locale from 'element-ui/lib/locale'

locale.use(lang)

Vue.use(Button)
Vue.use(Card)
Vue.use(Checkbox)
Vue.use(CheckboxButton)
Vue.use(CheckboxGroup)
Vue.use(Collapse)
Vue.use(CollapseItem)
Vue.use(Row)
Vue.use(Tabs)
Vue.use(TabPane)
